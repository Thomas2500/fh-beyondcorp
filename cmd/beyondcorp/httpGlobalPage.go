package main

func httpGlobalPageHeader(title string) string {

	if title == "" {
		title = "BeyondCorp"
	} else {
		title += " - BeyondCorp"
	}

	return `<html>
<head>
	<title>` + title + `</title>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, user-scalable=no" />
	<meta name="theme-color" content="#2196F3" />
	<link rel="stylesheet" href="/css/style.css" type="text/css" />
	<link rel="stylesheet" href="//thomas.bella.network/css/fonts.css" type="text/css" />
	<script defer src="//thomas.bella.network/js/fontawesome-all.min.js"></script>
	<link rel="shortcut icon" type="image/x-icon" href="//misc.bella.ml/img/beyondcorp.ico">
</head>
<body>
	<header>
		<a href="` + applicationSettings.BaseURL + `"><i class="fas fa-fw fa-ring"></i> BeyondCorp</a>
	</header>
`
}

func httpGlobalPageError(title string, message string) string {
	return `	<div class="content content-border">
		<div class="titlebar titlebar-red">
			<i class="fas fa-fw fa-exclamation-triangle"></i> ` + title + `
		</div>
		<div class="cardcontent">
			` + message + `
		</div>
	</div>`
}

func httpGlobalPageFooter() string {
	return `<footer>
		<i><small>This is a PoC and not a real product.</small></i> - <a href="https://imprint.unterhaltungsbox.com/">Impressum</a> - <a href="https://imprint.unterhaltungsbox.com/datenschutz">Datenschutzerklärung</a> - <a href="https://git.bella.network/FH-Burgenland/beyondcorp">Source Code</a>
	</footer>
	<script src="//thomas.bella.network/js/jquery.min.js"></script>
	<script src="/js/script.js"></script>
</body>
</html>`
}
