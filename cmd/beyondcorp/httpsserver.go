package main

import (
	"crypto/tls"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"
	"time"
)

var hostTarget = map[string]string{
	"time.fhsec.bella.ml":  "http://192.168.10.2",
	"git.fhsec.bella.ml":   "https://10.20.23.230",
	"other.fhsec.bella.ml": "http://192.168.10.3",
}

// beyondcorpHandler handles session request and resumption
func beyondcorpHandler(w http.ResponseWriter, r *http.Request) {
	// Got cookie from main page. Set it
	keys, ok := r.URL.Query()["cookie"]
	if ok && len(keys) == 1 {
		_, ok := applicationSessionStorage.Get(keys[0])
		if ok {
			http.SetCookie(w, &http.Cookie{
				Name:     "beyondcorp-session",
				Value:    keys[0],
				Expires:  time.Now().Add(time.Hour * 2),
				Secure:   true,
				HttpOnly: true,
				Path:     "/",
			})
			http.Redirect(w, r, "/", 307)
			return
		}
		rickroll(w)
		return
	}

	// We got no cookie, we have to request it from the base site
	http.Redirect(w, r, applicationSettings.BaseURL+"/pass-session?source="+r.Host, 307)
	w.Write([]byte(httpGlobalPageHeader("Redirecting") +
		httpGlobalPageError("Redirecting", "Redirecting you to "+applicationSettings.BaseURL+"/pass-session?source="+r.Host) +
		httpGlobalPageFooter()))

}

func globalHandler(w http.ResponseWriter, r *http.Request) {
	host := r.Host

	// Central entry point
	if host == "fhsec.bella.ml" || host == "fhsec.bella.ml:5443" {
		mainPage(w, r)
		return
	} else if host == "cert.fhsec.bella.ml" {
		certPage(w, r)
		return
	}

	// Check beyondcorp-session cookie
	if !checkValidSession(r) {
		// Redirect to get session cookie. Abort further processing to nw allow hidden attacks to target
		http.Redirect(w, r, "/beyondcorp-session", 307)
		return
	}

	cs := getCurrentSession(r)

	// Other host. Check cookie if user is authenticated
	if target, ok := hostTarget[host]; ok {
		remoteURL, err := url.Parse(target)
		if err != nil {
			log.Println("target parse fail:", err)
			return
		}

		r.Header.Del("beyondcorp-session")

		// Add userid to identify the user
		r.Header.Del("X-BEYONDCORP-UID")
		r.Header.Add("X-BEYONDCORP-UID", strconv.FormatUint(cs.UserID, 10))

		// Build reverse proxy for this connection
		proxy := httputil.NewSingleHostReverseProxy(remoteURL)
		// Modify request data
		proxy.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		proxy.ServeHTTP(w, r)

		// Update current session LastUse timestamp
		go updateCurrentSession(r)

		return
	}

	// Something went wrong
	rickroll(w)
}

func rickroll(w http.ResponseWriter) {
	// Target host is unknown. Someone is trying to access a resource which doesn't exist. Rickroll
	w.Write([]byte(httpGlobalPageHeader("Access Denied") +
		httpGlobalPageError("Access Denied", "You are not allowed to view the requested page.<br>\n<br>\n"+`<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/dQw4w9WgXcQ?controls=0&autoplay=1&start=18" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`) +
		httpGlobalPageFooter()))
}

func httpsServer() {
	var err error
	tlsConfig := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.X25519, tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_FALLBACK_SCSV,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
	}
	tlsConfig.Certificates = make([]tls.Certificate, 1)

	// First key is also fallback
	if _, err := os.Stat("/etc/beyondcorp/cert0.pem"); os.IsNotExist(err) {
		// Fallback to dev environment
		if _, err := os.Stat("/home/thomas2500/Sync/Projekte/studium/beyondcorp/cert0.pem"); os.IsNotExist(err) {
			log.Fatalln("The file /etc/beyondcorp/cert0.pem does not exist.")
		} else {
			tlsConfig.Certificates[0], err = tls.LoadX509KeyPair("/home/thomas2500/Sync/Projekte/studium/beyondcorp/cert0.pem", "/home/thomas2500/Sync/Projekte/studium/beyondcorp/cert0.key")
			if err != nil {
				log.Fatal(err)
			}
		}
	} else {
		tlsConfig.Certificates[0], err = tls.LoadX509KeyPair("/etc/beyondcorp/cert0.pem", "/etc/beyondcorp/cert0.key")
		if err != nil {
			log.Fatal(err)
		}
	}
	if _, err := os.Stat("/etc/beyondcorp/cert1.pem"); os.IsNotExist(err) {
		if _, err := os.Stat("/home/thomas2500/Sync/Projekte/studium/beyondcorp/cert1.pem"); os.IsNotExist(err) {
		} else {
			tcert, err := tls.LoadX509KeyPair("/home/thomas2500/Sync/Projekte/studium/beyondcorp/cert1.pem", "/home/thomas2500/Sync/Projekte/studium/beyondcorp/cert1.key")
			if err != nil {
				log.Fatal(err)
			}
			tlsConfig.Certificates = append(tlsConfig.Certificates, tcert)
		}
	} else {
		tcert, err := tls.LoadX509KeyPair("/etc/beyondcorp/cert1.pem", "/etc/beyondcorp/cert1.key")
		if err != nil {
			log.Fatal(err)
		}
		tlsConfig.Certificates = append(tlsConfig.Certificates, tcert)
	}
	/*
		tlsConfig.Certificates[1], err = tls.LoadX509KeyPair("test1.pem", "key.pem")
		if err != nil {
			log.Fatal(err)
		}
		tlsConfig.Certificates[2], err = tls.LoadX509KeyPair("test2.pem", "key.pem")
		if err != nil {
			log.Fatal(err)
		}
	*/
	tlsConfig.BuildNameToCertificate()

	// Global override as handler for sessions
	http.HandleFunc("/beyondcorp-session", beyondcorpHandler)

	// Default path which matches everything
	http.HandleFunc("/", globalHandler)
	server := &http.Server{
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
		TLSConfig:      tlsConfig,
	}

	listener, err := tls.Listen("tcp", ":5443", tlsConfig)
	if err != nil {
		log.Fatal(err)
	}
	log.Fatal(server.Serve(listener))
}

func httpRedirectServer() {
	http.ListenAndServe(":5080", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		target := "https://" + req.Host + req.URL.Path
		if len(req.URL.RawQuery) > 0 {
			target += "?" + req.URL.RawQuery
		}
		http.Redirect(w, req, target, 307)
	}))
}
