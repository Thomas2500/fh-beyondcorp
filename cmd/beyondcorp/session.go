package main

import (
	"math/rand"
	"net/http"
	"sync"
	"time"
)

var applicationSessionStorage userSession

type userSession struct {
	Sessions map[string]singleUserSession
	rwLock   sync.RWMutex
}
type singleUserSession struct {
	UserID   uint64
	Creation time.Time
	LastUse  time.Time
}

func (s *userSession) Init() {
	s.Sessions = make(map[string]singleUserSession)
	go s.GC()
}
func (s *userSession) Get(sessionToken string) (singleUserSession, bool) {
	s.rwLock.RLock()
	defer s.rwLock.RUnlock()
	val, found := s.Sessions[sessionToken]
	return val, found
}
func (s *userSession) Create(userID uint64) (sessionToken string) {
	s.rwLock.Lock()
	defer s.rwLock.Unlock()
	sessionToken = randomString(64)
	s.Sessions[sessionToken] = singleUserSession{
		UserID:   userID,
		Creation: time.Now(),
		LastUse:  time.Now(),
	}
	return sessionToken
}
func (s *userSession) UpdateLastUse(sessionToken string) {
	s.rwLock.Lock()
	defer s.rwLock.Unlock()
	oldData := s.Sessions[sessionToken]
	oldData.LastUse = time.Now()
	s.Sessions[sessionToken] = oldData
}
func (s *userSession) GC() {
	s.rwLock.Lock()
	defer s.rwLock.Unlock()
	// Delete old sessions
	for key, value := range s.Sessions {
		if time.Since(value.LastUse).Seconds() > 60*60 {
			delete(s.Sessions, key)
		}
	}
	// Execute GC every 5 minutes
	time.AfterFunc(time.Duration(time.Minute*5), func() { s.GC() })
}

func init() {
	// Initiate random number generator
	rand.Seed(time.Now().UnixNano())

	// Initiate object
	applicationSessionStorage = userSession{}
	applicationSessionStorage.Init()
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=-_()!?")

func randomString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func checkValidSession(r *http.Request) bool {
	sessionCookie, err := r.Cookie("beyondcorp-session")
	if err != nil {
		return false
	}
	if len(sessionCookie.Value) == 0 {
		return false
	}

	_, found := applicationSessionStorage.Get(sessionCookie.Value)
	return found
}

func getCurrentSession(r *http.Request) singleUserSession {
	sessionCookie, _ := r.Cookie("beyondcorp-session")
	data, _ := applicationSessionStorage.Get(sessionCookie.Value)
	return data
}

func updateCurrentSession(r *http.Request) {
	sessionCookie, _ := r.Cookie("beyondcorp-session")
	applicationSessionStorage.UpdateLastUse(sessionCookie.Value)
}
