package main

/*
func init() {
	bytes, _ := bcrypt.GenerateFromPassword([]byte("demo"), 14)
	fmt.Println(string(bytes))
	os.Exit(0)
}
*/

func main() {
	go httpRedirectServer()
	go httpsServer()

	// Endless wait to serve http and https servers
	select {}
}
