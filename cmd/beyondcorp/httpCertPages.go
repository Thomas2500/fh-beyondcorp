package main

import "net/http"

func certPage(w http.ResponseWriter, r *http.Request) {

	switch r.URL.Path {
	case "/ca.crt":
		caCert(w)
		return
	case "/check.json":
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write([]byte("{\"request\": \"ok\"}"))
	case "/check.js":
		w.Header().Set("Content-Type", "application/javascript")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Write([]byte("var loadWasSuccessfull = true;"))
	}
}

func caCert(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/x-x509-ca-cert")
	w.Write([]byte(`-----BEGIN CERTIFICATE-----
MIIGRjCCBC6gAwIBAgIJAJBHndBRv5suMA0GCSqGSIb3DQEBCwUAMIGvMQswCQYD
VQQGEwJBVDEQMA4GA1UECAwHQXVzdHJpYTETMBEGA1UEBwwKU2llZ2VuZmVsZDEh
MB8GA1UECgwYVW50ZXJoYWx0dW5nc2JveCBSb290IENBMS4wLAYDVQQLDCVVbnRl
cmhhbHR1bmdzYm94IENlcnRpZmljYXRlIFNlcnZpY2VzMSYwJAYJKoZIhvcNAQkB
FhdjYUB1bnRlcmhhbHR1bmdzYm94LmNvbTAeFw0xNjA4MDYxMzI1MTRaFw0zNjA4
MDExMzI1MTRaMIGvMQswCQYDVQQGEwJBVDEQMA4GA1UECAwHQXVzdHJpYTETMBEG
A1UEBwwKU2llZ2VuZmVsZDEhMB8GA1UECgwYVW50ZXJoYWx0dW5nc2JveCBSb290
IENBMS4wLAYDVQQLDCVVbnRlcmhhbHR1bmdzYm94IENlcnRpZmljYXRlIFNlcnZp
Y2VzMSYwJAYJKoZIhvcNAQkBFhdjYUB1bnRlcmhhbHR1bmdzYm94LmNvbTCCAiIw
DQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAOZ8/SYjV6oZAXlNlMbzwSh0VGup
zS6AVEgF55R101FRJWOJwJrCkN6fd52f6vW23YxqweUCTj8DjxSenDUqFfRQqGbU
XCAHEOwTqBRoxf5XPpewipTW+9XDBHRySsJuG+oCIt0jc9bMNMRjqoN/Ooytr2kt
9bE2k4zLyLJWEMiPCgqgzI8bcfhUmpolWDu6OD/p6Y+zDpK+5iaDHsS5z82E4Kut
ikIzOc8GIbzmyXhAu1sg9foLHFsULK88IYXOSaY6jEK6le6syhMgCFEB01cijMxP
vj5qs48IzCeVW7LtFHTox99zi7g6el2uO4GASm6UlVmuTPQ1Kje26y2Pepo9VRZT
kZCq8Zk46oZeZiQeVndhoQnp4vOF7Vii2n/FXoUDM+dOp0S95GyisE4qS5tQctEH
5G8M7ePxjO7lp7avwkAwy4I0Go+Fc0oOdO35E/YHwveQSvBQte7LJ9F6Fuo/G8Wc
yXKrlxPW3HDjZUghyfJ0K9rkipaqfdakBTgRfz4qaS1NY2jdca4iBJKGFOuIO4vW
JO9ZW6IkmG1OirNnoLNioZgLgreKfZVcrW7J1YP61eD0RDuKE6GeBYq9xQYnJEKx
M4d/MW9MQ13/qM6KuZDLNxLrhJ9Y2EAYeuEwzuBETOBiiOw/9hPo5eJ8XVGK3pgh
ude2Tm1T9TyA0QkRAgMBAAGjYzBhMB0GA1UdDgQWBBSbwZrOieoB5/1dzkFAZ7kI
O+g0qjAfBgNVHSMEGDAWgBSbwZrOieoB5/1dzkFAZ7kIO+g0qjAPBgNVHRMBAf8E
BTADAQH/MA4GA1UdDwEB/wQEAwIBhjANBgkqhkiG9w0BAQsFAAOCAgEAcq/aCf1X
MBJFEBtgJ1CTgb50Udt2yrdvPUbWXijHaiIMGfH759MZ3yBx97nKvl1q1iooBOp1
oCI0B3h8EnJOBO+DWRo8SahIb979aTb66/BlTa9C8BRCFNPPuBWH0ZNJxNIA2fZU
zHssLJ/dz2NdUUmuitkwvQPeSYz7+sQnvJhKA7SBov+DNH9hsOccIMAMyyGuwX7D
KSEDERYunVHCqarOOGXL2phfQ9hjr0aSbJueApGNSasOzl59MnM2ci3uceSkqVF0
2fmooc4PrMnb784XxrpMI9s36Umt/4fRE6h/5O3TOjcqcVDgjYtCdevo21c+mwik
ItT0QU3GpuNVHxqk0pazvUgYnTX1kdM6YtA3V5gb/ZL8jv8N7LgJKd+17vCaClQJ
Kxd6wwKXtGU6l8wloQnt2etolWJpMmYzrEttDfHBqqImW+uoI70THukYpacCRkjN
qJkCUe0jP8bG1RNAYM/NmLVttwLNKkjPyXUeGK28w7c39SwcFNeB9ygBNGjBrOHM
oPtFu6iUSfbZRZK2CnvSCdZC8mb3uhe7whOioC0SkivcFGD7tRRxUUa4/ZNDMZDz
JwXqdVmTz2jSA065kV7YSo1wUpbrpNBYNmuuL5Krj5DxV3OIyqPI3XmUbZQBksDg
OxycASIY3oeZ3DttiJyotFr+AAdUeF+5SEs=
-----END CERTIFICATE-----
`))
}
