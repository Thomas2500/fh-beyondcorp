package main

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func mainPage(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Strict-Transport-Security", "max-age=86400")
	w.Header().Add("X-Frame-Options", "SAMEORIGIN")
	w.Header().Add("X-XSS-Protection", "1; mode=block")
	w.Header().Add("Expect-CT", "max-age=604800")
	w.Header().Add("Expect-Staple", "max-age=604800")

	switch r.URL.Path {

	case "/cert.crt":
		caCert(w)
		break
	case "/login":
		err := r.ParseForm()
		if err != nil {
			// Chant parse form. Abort request
			return
		}

		// Parse fields
		username := r.Form.Get("username")
		password := r.Form.Get("password")
		if len(username) == 0 || len(password) == 0 {
			w.Write([]byte(httpGlobalPageHeader("Login failure") +
				httpGlobalPageError("Login failure", "The username or password you entered is incorrect.") +
				httpGlobalPageFooter()))
			return
		}

		var authUser uint64

		// Find user from users table
		for _, singleUser := range applicationSettings.Users {
			if singleUser.Username == strings.ToLower(username) {
				err := bcrypt.CompareHashAndPassword([]byte(singleUser.Password), []byte(password))
				if err != nil {
					// Password is not correct. Print error message
					w.Write([]byte(httpGlobalPageHeader("Login failure") +
						httpGlobalPageError("Login failure", "The username or password you entered is incorrect.") +
						httpGlobalPageFooter()))
					return
				}
				authUser = singleUser.ID
			}
		}

		// Abort further processing because data was not correct
		if authUser == 0 {
			w.Write([]byte(httpGlobalPageHeader("Login failure") +
				httpGlobalPageError("Login failure", "The username or password you entered is incorrect.") +
				httpGlobalPageFooter()))
			return
		}

		// Login was successfull - attach cookie and redirect to overview page
		newSessionCookie := applicationSessionStorage.Create(authUser)
		http.SetCookie(w, &http.Cookie{
			Name:     "beyondcorp-session",
			Value:    newSessionCookie,
			Expires:  time.Now().Add(time.Hour * 2),
			Secure:   true,
			HttpOnly: true,
			Path:     "/",
		})
		http.Redirect(w, r, "/overview", 307)

	case "/":

		// Redirect to overvierw if user is logged in
		valid := checkValidSession(r)
		if valid {
			http.Redirect(w, r, "/overview", 307)
			return
		}

		w.Write([]byte(httpGlobalPageHeader("Login") + `
	<div class="content content-border">
		<div class="titlebar">
			<i class="fas fa-fw fa-sign-in"></i> Login
		</div>
		<div class="content-login">
			<div class="login-item">
				<form method="POST" action="/login" class="form form-login">
					<div class="form-field">
						<label class="user" for="login-username"><i class="fas fa-fw fa-user"></i></label>
						<input id="login-username" type="text" name="username" class="form-input" placeholder="E-Mail" autofocus="autofocus" required="required">
					</div>
					<div class="form-field">
						<label class="password" for="login-password"><i class="fas fa-fw fa-key"></i></label>
						<input id="login-password" type="password" name="password" class="form-input" placeholder="Password" required="required">
					</div>
					<div class="form-field access">
						<input type="submit" value="Login">
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="cert-download">
		<i class="fas fa-fw fa-file-certificate"></i> Download SSL certificate
	</div>
` + httpGlobalPageFooter()))

	case "/css/style.css":
		w.Header().Set("Content-Type", "text/css")
		w.Write([]byte(`head, body {
	margin: 0px;
	padding: 0px;
	font-family: "Roboto", sans-serif;
	background-color: #FAFAFA !important;
	font-size: 15px;
	width: 100%;
}
header {
	background-color: #2196F3;
	color: #FFFFFF;
	line-height: 64px;
	font-size: 24px;
	box-shadow:0 2px 5px 0 rgba(0,0,0,0.16);
	margin-bottom: 32px;
	text-align: center;
}
header > a {
	text-decoration: none;
	color: white;
}
.content {
	margin-left: auto;
	margin-right: auto;
	max-width: 650px;
	padding: 5px 10px;
	background-color: white;
}
.content-border {
	border-radius: 2px;
	background-color: #FFFFFF;
	overflow: hidden;
	box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
	color: rgba(0, 0, 0, 0.87);
	padding: 0px;
}
.content > .titlebar {
	background-color: #2196F3;
	color: white;
	font-size: 24px;
	border-bottom: 1px solid rgba(0, 0, 0, 0.16);
	box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16);
	padding: 10px 14px;
}
.content > .titlebar-red {
	background-color: #E64A19;
}
.content > .cardcontent {
	padding: 20px;
}
footer {
	position: absolute;
	bottom: 0px;
	left: 0px;
	right: 0px;
	margin-top: 15px;
	background-color: #FFFFFF;
	overflow: hidden;
	box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
	color: rgba(0, 0, 0, 0.87);
	text-align: center;
	padding: 7px 10px;
}
a {
	color: #0D47A1;
}

.content-login {
	padding: 25px 10px;
	display: flex;
	flex-direction: column;
	background-color: #EEE;
}
.login-item {
	color: #ffff;
	margin: 15px 20px 0;
	border-radius: 3px;
}
input {
	border: 0;
	color: inherit;
	font: inherit;
	margin: 0;
	outline: 0;
	padding: 0;
	transition: background-color .3s;
}
.form input[type="password"], .form input[type="text"], .form input[type="submit"] {
	width: 100%;
}
.form-login label,
.form-login input[type="text"],
.form-login input[type="password"],
.form-login input[type="submit"] {
	border-radius: 0.25rem;
	padding: 1rem;
	color: #3A3F44;
}
.form-login label {
	background-color: #2196F3;
	border-bottom-right-radius: 0;
	border-top-right-radius: 0;
	padding-left: 1.25rem;
	padding-right: 1.25rem;
	color: white;
	font-size: 16px;
}
.form-login input[type="text"], .form-login input[type="password"] {
	background-color: #ffffff;
	border-bottom-left-radius: 0;
	border-top-left-radius: 0;
}
.form-login input[type="text"]:focus, .form-login input[type="text"]:hover, .form-login input[type="password"]:focus, .form-login input[type="password"]:hover {
	background-color: #fff;
}
.form-login input[type="submit"] {
	background-color: #00B9BC;
	color: #eee;
	font-weight: bold;
	text-transform: uppercase;
}
.form-login input[type="submit"]:focus, .form-login input[type="submit"]:hover {
	background-color: #197071;
}
.form-field {
	display: flex;
	margin-bottom: 2rem;
}

.loginform .title {
	flex-grow: 1;
	flex-basis: 30%;
	font-weight: bold;
	text-align: right;
	line-height: 44px;
}
.loginform .content {
	flex-grow: 2;
	flex-basis: 60%;
}
.loginform .content input {
	padding: 6px 12px;
	height: 34px;
	border-radius: 4px;
	border: 1px solid #ccc;
	box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	width: 300px;
}
.content .access {
	text-align: center;
	margin-top: 15px;
	margin-bottom: 0px;
}
.content .access input {
	color: #fff;
	border: 1px solid;
	background-color: #204d74;
	border-color: #122b40;
	display: inline-block;
	padding: 8px 14px;
	cursor: pointer;
	border-radius: 4px;
}
.content .access input:hover {
	color: #fff;
	background-color: #286090;
	border-color: #204d74;
}
.cert-download {
	max-width: 300px;
	margin-left: auto;
	margin-right: auto;
	margin-top: 25px;
	background-color: #2196F3;
	color: white;
	padding: 10px 15px;
	font-size: 18px;
	box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16);
	text-align: center;
	cursor: pointer;
	border-radius: 6px;
}
`))
	case "/js/script.js":
		w.Header().Set("Content-Type", "application/javascript")
		w.Write([]byte(`$(function(){
	$(".cert-download").click(function(){
		location.href = '/cert.crt';
		$(".cert-download").hide();
	});
	$.get('https://cert.fhsec.bella.ml/check.json', function(data){
		if (data.request == "ok") {
			$(".cert-download").hide();
		}
	}, 'json');
});
`))

	case "/overview":
		valid := checkValidSession(r)
		if !valid {
			http.Redirect(w, r, "/", 307)
			return
		}

		currentSession := getCurrentSession(r)
		userAccount := userData{}
		for _, value := range applicationSettings.Users {
			if value.ID == currentSession.UserID {
				userAccount = value
				break
			}
		}

		w.Write([]byte(httpGlobalPageHeader("Overview") + `
	<div class="content content-border">
		<div class="titlebar">
			<i class="fas fa-fw fa-user"></i> Welcome #` + strconv.FormatUint(currentSession.UserID, 10) + `: ` + userAccount.Username + `
		</div>
		<div class="cardcontent">
Available services in this BeyondCorp:<br>
<ul>`))
		for index := range hostTarget {
			w.Write([]byte(`<li><a href="https://` + index + `">` + index + `</a></li>`))
		}
		w.Write([]byte(`</ul>
		</div>
	</div>
			` +
			httpGlobalPageFooter()))

	case "/pass-session":
		valid := checkValidSession(r)
		if !valid {
			http.Redirect(w, r, "/", 307)
			w.Write([]byte(httpGlobalPageHeader("Invalid Session") +
				httpGlobalPageError("Invalid Session", "Your session is invalid. You will be redirected") +
				httpGlobalPageFooter()))
			return
		}

		// Get redirect target
		keys, ok := r.URL.Query()["source"]
		if !ok || len(keys[0]) < 1 {
			w.Write([]byte(httpGlobalPageHeader("Source missing") +
				httpGlobalPageError("Source missing", "Please do not call this resource directly.") +
				httpGlobalPageFooter()))
			return
		}

		// Check if target is valid
		target := ""
		for singleSite := range hostTarget {
			if singleSite == keys[0] {
				target = singleSite
			}
		}

		// Target was not found
		if len(target) == 0 {
			rickroll(w)
			return
		}

		sessionCookie, _ := r.Cookie("beyondcorp-session")
		http.Redirect(w, r, "https://"+target+"/beyondcorp-session?cookie="+sessionCookie.Value, 307)
		w.Write([]byte(httpGlobalPageHeader("Redirecting") +
			httpGlobalPageError("Redirecting", "Redirecting to https://"+target+"/beyondcorp-session?cookie="+sessionCookie.Value) +
			httpGlobalPageFooter()))
	}
}
