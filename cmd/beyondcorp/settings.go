package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type programSettings struct {
	Users   []userData
	BaseURL string
}

type userData struct {
	Username string
	Password string
	ID       uint64
}

var applicationSettings programSettings

func init() {
	content, err := ioutil.ReadFile("/etc/beyondcorp/config.json")
	if err != nil {
		content, err = ioutil.ReadFile("config.json")
		if err != nil {
			log.Println("Can't read settings from config file. Assuming defaults")
			return
		}
	}

	if err = json.Unmarshal(content, &applicationSettings); err != nil {
		log.Println("Error reading settings from file")
		log.Println(err)
	}
}
