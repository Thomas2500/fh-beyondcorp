# BeyondCorp

## TODO

- Logout
- Configure TLS certs and available internal URLs with config.json

## Example config

```json
{
    "BaseURL": "https://fhsec.bella.ml/",
    "Users": [
        {
            "ID": 1,
            "Username": "demo@example.com",
            "Password": "$2a$14$m8Pmv7/aluK6rGOVtijqlOsxtgBPjRhNV6ifKtxqDiaxSHUNjbJLK"
        }
    ]
}
```

The password for the user `demo@example.com` is `demo`. The password is based on 14 rounds bcrypt.
