#!/usr/bin/env bash

set -e

# Arguments this script was called
# First argument: Destination folder where builds are stored, when empty current folder is used
PRJ_ROOT=$1
if [ -z "${PRJ_ROOT}" ]; then
	PRJ_ROOT="./"
fi
# Second argument: Location of main program, can be empty if folder itself or something like "cmd/program"
MAIN_PROG="./${2}"

# Current commit hash with 8 chars
COMMIT_HASH=$(git rev-parse --short=8 HEAD 2>/dev/null || true)
# Use current time as version
VERSION=$(date "+%Y%m%d%H%I%S")
# Alternatively use git tag
#VERSION=$(git describe --tags --dirty)
# Current date
BUILD_DATE=$(date "+%Y-%m-%d")

# Set project root if not already set
if [ "$PRJ_ROOT" == "" ]; then
	PRJ_ROOT="$(pwd)"
fi

# Set current commit if not found with git tools
if [ "$COMMIT_HASH" == "" ]; then
	COMMIT_HASH="00000000"
fi

# Go build arguments
GO_BUILD_CMD="go build -a -installsuffix cgo"
GO_BUILD_LDFLAGS="-s -w -X main.commitHash=${COMMIT_HASH} -X main.programVersion=${VERSION} -X main.buildDate=${BUILD_DATE}"

# Declare variable if not run from GitLab CI
if [[ -z "${CI_PROJECT_NAME}" ]]; then
	CI_PROJECT_NAME="manualbuild"
fi

# List of available platforms and archs is available at https://github.com/golang/go/blob/master/src/go/build/syslist.go
# Most common values are: linux windows
# Multiple values separated with space
BUILD_PLATFORMS="linux"
# Most common values are: amd64 386 arm arm64
BUILD_ARCHS="amd64"
# Attention with ARM: There are multiple versions possible which sould be
# definied as GOARCH=X. X can be 5, 6 or 7.
# armel kernel uses ARMv5, armhf uses ARMv6+
# Examples for Raspberry: https://de.wikipedia.org/wiki/Raspberry_Pi#Raspberry_Pi
# Examples:
#  Raspberry Pi Zero - 1 B+: ARMv6
#  Raspberry Pi 2 B: ARMv7
#  Raspberry Pi 3: ARMv8, but 32bit is used. So ARMv7

# Create target dir
mkdir -p "${PRJ_ROOT}"

for OS in ${BUILD_PLATFORMS[@]}; do
	for ARCH in ${BUILD_ARCHS[@]}; do
		NAME="${CI_PROJECT_NAME}-${OS}-${ARCH}"
		if [[ "${OS}" == "windows" ]]; then
			# Skip arm and arm64 if using windows
			# TODO: windows/arm is currently in developement for Windows IoT Core: https://github.com/golang/go/issues/26148
			if [[ "${ARCH}" == "arm" ]] || [[ "${ARCH}" == "arm64" ]]; then
				continue
			fi
			# Windows has .exe files as executables
			NAME="${NAME}.exe"
		fi
		echo "Building for ${OS}/${ARCH}"
		if [[ "${ARCH}" == "arm" ]]; then
			GOARM=6 GOARCH=${ARCH} GOOS=${OS} CGO_ENABLED=0 ${GO_BUILD_CMD} -ldflags "${GO_BUILD_LDFLAGS}"\
				-o "${PRJ_ROOT}/${NAME}6" ${MAIN_PROG}
			GOARM=7 GOARCH=${ARCH} GOOS=${OS} CGO_ENABLED=0 ${GO_BUILD_CMD} -ldflags "${GO_BUILD_LDFLAGS}"\
				-o "${PRJ_ROOT}/${NAME}7" ${MAIN_PROG}
		else
			# Add GODEBUG=tls13=1 to enable TLSv1.3 support (opt-in for now)
			GOARCH=${ARCH} GOOS=${OS} CGO_ENABLED=0 GODEBUG=tls13=1 ${GO_BUILD_CMD} -ldflags "${GO_BUILD_LDFLAGS}"\
				-o "${PRJ_ROOT}/${NAME}" ${MAIN_PROG}
		fi
	done
done
